# Recepies

A project to evaluate development approach.


# Introduction
This is a simple Recipies app. The following are the requirements:

- Login
	- User can register
	- User can login
	
- User Dashboard
	- User can see a list off all recipies with following info:
		- a picture (random from Internet)
		- a title
	- User can create recipies
	- User can edit only own recipies
	- User can click any recipe and a details page opens up with the following details
		- a picture (random from Internet)
		- a title
		- a description
		
	