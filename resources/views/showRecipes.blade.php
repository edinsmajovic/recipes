<!DOCTYPE html>
<html>
<title>Recipes</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<!-- Custom Styles -->
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">

<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-padding w3-card" style="letter-spacing:4px;">
    <a href="#home" class="title w3-bar-item w3-button">Recipes</a>
    <!-- Right-sided navbar links. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{route('add-recipe-form')}}">Add Recipe</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                @else
                    <a href="{{ route('login') }}">Login</a>
                    <a href="{{ route('register') }}">Register</a>
                @endauth
            </div>
        @endif
    </div>
  </div>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1600px;min-width:500px" id="home">
  <img class="w3-image" src="https://www.w3schools.com/w3images/hamburger.jpg" alt="Hamburger Catering" width="1600" height="800">
  <div class="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 class="w3-xxlarge">Make it OWN</h1>
  </div>
</header>

<!-- Page content -->
<div class="w3-content" style="max-width:1100px">

  <!-- About Section -->
  @foreach($recipes as $recipe)
  <div class="w3-row w3-padding-64">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="{{ Storage::url($recipe->photo) }}" class="w3-round w3-image" alt="Recipe" width="300">
      <h1 class="w3-center">{{$recipe->title}}</h1><br>
      <a class="btn btn-primary" href="{{route('show-details',['id'=> $recipe->id])}}">Details</a>
    </div>
  </div>
  
  <!--<hr>-->
  @endforeach
  <div class="list"> 
    {{$recipes->links()}}
  </div>
<!-- End page content -->
</div>

</body>
</html>