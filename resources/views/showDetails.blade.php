<div class="w3-row w3-padding-64">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="{{$recipe->photo}}" class="w3-round w3-image" alt="Recipe" width="600" height="750">
    </div>

      
    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">{{$recipe->title}}</h1><br>
      <p class="w3-large">{{$recipe->description}}</p>
    </div>
</div>

@auth
<a class="btn btn-primary" href="{{route('edit-recipe-form',['id'=> $recipe->id])}}">Edit</a>
@endauth