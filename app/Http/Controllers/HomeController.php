<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = DB::table('users')->where('id',$id)->first();
        $recipes = DB::table('recipes')->simplePaginate(3);
        // dd($recipes);
        
        return redirect()->route('show-recipes')->with(
            ['user' => $user],
            ['recipes' => $recipes]
        );
    }
}
