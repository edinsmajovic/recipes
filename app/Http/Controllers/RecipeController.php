<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RecipeController extends Controller
{
    public function showRecipes(){
        
        $recipes = DB::table('recipes')->simplePaginate(3);
//        print_r($recipes);

        return view('showRecipes')->with(['recipes' => $recipes]);
    }

    public function showAddRecipeForm(){
        return view('addRecipeForm');
    }
    
    public function addRecipe(Request $request){

        $input = $request->all();
        
        $title = $input['title'];
        $desc = $input['desc'];
        $photo = $request->file('photo')->store('public');


        $user = User::find(Auth::id());
        $user_id = $user['id'];
        
        DB::table('recipes')->insert([
            'title' => $title,
            'description' => $desc,
            'photo' => $photo,
            'user_id' => $user_id
        ]);
        
        return redirect()->route('show-recipes');
    }

    public function showDetails($id) {

        $recipe = DB::table('recipes')->where('id','like',$id)->get();

        return view('showDetails')->with(['recipe'=>$recipe[0]]);
    }

    public function showEditRecipeForm($id){
        $recipe = DB::table('recipes')->where('id','like',$id)->get();

        return view('editrecipeform')->with(['recipe'=>$recipe[0]]);
    }

    public function editRecipe($id, Request $request){

        $input = $request->all();

        $title = $input['title'];
        $desc = $input['desc'];

        if (!isset($input['photo'])) {
            $photo = 'https://taste.co.za/wp-content/themes/taste.co.za/img/thumbnail-default.png';
        } else {
            $photo = $input['photo'];
        }

        $user = User::find(Auth::id());
        $user_id = $user['id'];
        
        DB::table('recipes')->where('id','like',$id)->update([
            'title' => $title,
            'description' => $desc,
            'photo' => Storage::put('storage', $photo),
            'user_id' => $user_id
        ]);

        return redirect()->route('show-recipes');
    }
}
