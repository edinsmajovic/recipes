<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
});

Auth::routes();

Route::get('/home/{id}', 'HomeController@index')->name('home');

Route::get('/showrecipeslist')->name('show-recipes')->uses('RecipeController@showRecipes');

Route::get('/showAddRecipeForm')->name('add-recipe-form')->uses('RecipeController@showAddRecipeForm');

Route::post('/addNewRecipe')->name('add-recipe')->uses('RecipeController@addRecipe');

Route::get('/details/{id}')->name('show-details')->uses('RecipeController@showDetails');

Route::get('/editrecipeform/{id}')->name('edit-recipe-form')->uses('RecipeController@showEditRecipeForm');

Route::post('/edit/{id}')->name('edit-recipe')->uses('RecipeController@editRecipe');